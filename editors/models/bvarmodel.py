from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from ..datatypes import BehaviourVariable
from collections import OrderedDict


class BehaviourVariableModel(QAbstractTableModel):
    """
    bvar table model

    """

    def __init__(self, parent=None):
        super(BehaviourVariableModel, self).__init__(parent)
        self.Header = ['Behaviour Variable', 'Value']
        self.inventoryRecords = []

    def update_table(self, jsonData):
        """
        change the inventory data loaded into the model

        :param jsonData:
        :type jsonData: dict
        :return:
        :rtype:
        """
        if 'behaviorVariables' not in jsonData:
            return False
        self.beginResetModel()
        self.inventoryRecords = []
        for item in jsonData['behaviorVariables']:
            val = BehaviourVariable()
            val.fromJson(item)
            self.inventoryRecords.append(val)
        self.endResetModel()
        return True
        # print 'Table Update Op took {0:02f} seconds'.format(time.time() - fl_st)

    def changeBvars(self, lstBvars):
        self.beginResetModel()
        self.inventoryRecords = []
        for item in lstBvars:
            self.inventoryRecords.append(item)
        self.endResetModel()

    def export_table(self):
        """

        :return:
        :rtype: OrderedDict
        """
        data = OrderedDict()
        data['behaviorVariables'] = []
        for item in self.inventoryRecords:
            data['behaviorVariables'].append(item.toJson())
        return data

    def refresh(self):
        """
        refresh the table

        :return:
        :rtype:
        """
        self.beginResetModel()
        self.endResetModel()

    def rowCount(self, *args, **kwargs):
        return len(self.inventoryRecords)

    def columnCount(self, *args, **kwargs):
        return len(self.Header)

    def flags(self, index):
        """
        set flags for rows and columns in the table

        :param index: the index of the cell
        :type index:
        :return:
        :rtype:
        """
        if not index.isValid():
            return None
        if index.column() != 1:
            return super(BehaviourVariableModel, self).flags(index)
        else:
            # make the current value column editable
            flags = super(BehaviourVariableModel, self).flags(index)
            flags |= Qt.ItemIsEditable
            return flags

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role not in [Qt.DisplayRole, Qt.EditRole]:
            return None
        record = self.inventoryRecords[index.row()]
        if index.column() == 0:
            return record.key
        elif index.column() == 1:
            return str(record.value)
        else:
            return None

    def setData(self, index, value, role):
        if not index.isValid():
            return False
        try:
            if index.column() == 1:
                record = self.inventoryRecords[index.row()]
                if record.valType == BehaviourVariable.boolType:
                    if value.lower() in ['true', '1']:
                        record.value = True
                    elif value.lower() in ['false', '0']:
                        record.value = False
                    else:
                        return False
                elif record.valType == BehaviourVariable.intType:
                    try:
                        val = int(value)
                        record.value = val
                    except ValueError:
                        return False
                elif record.valType == BehaviourVariable.floatType:
                    try:
                        val = float(value)
                        record.value = val
                    except ValueError:
                        return False
                elif record.valType == BehaviourVariable.stringType:
                    record.value = value
                self.dataChanged.emit(index, index)
                return True
        except ValueError:
            return False

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.Header[col]
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col
        return None

    def deleteItems(self, lstItems):
        lst_remove = []
        for idx in lstItems:
            if idx.isValid():
                lst_remove.append(self.inventoryRecords[idx.row()])
        self.beginResetModel()
        for item in lst_remove:
            self.inventoryRecords.remove(item)
        self.endResetModel()

    def addItem(self, name, dtype, value):
        record = BehaviourVariable()
        record.key = name
        record.valType = dtype
        if record.valType == BehaviourVariable.boolType:
            if value.lower() in ['true', '1']:
                record.value = True
            elif value.lower() in ['false', '0']:
                record.value = False
            else:
                return False
        elif record.valType == BehaviourVariable.intType:
            try:
                val = int(value)
                record.value = val
            except ValueError:
                return False
        elif record.valType == BehaviourVariable.floatType:
            try:
                val = float(value)
                record.value = val
            except ValueError:
                return False
        elif record.valType == BehaviourVariable.stringType:
            record.value = value
        self.beginResetModel()
        self.inventoryRecords.append(record)
        self.endResetModel()
        return True