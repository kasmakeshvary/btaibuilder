from collections import OrderedDict
import json

from .selector import Selector
from .behaviourvariable import BehaviourVariable


class UnitAiOverrideDef(object):

    def __init__(self):
        self.Id = ''
        self.Priority = 0
        self.TreeRootName = ''
        self.Selectors = []  # type: list[Selector]
        self.BehaviourVars = []  # type: list[BehaviourVariable]
        self.AddedInfluenceFactors = []
        self.RemovedInfluenceFactors = []

    def fromJson(self, jsonData):
        """

        :param jsonData:
        :type jsonData: OrderedDict
        :return:
        :rtype:
        """
        self.Id = jsonData['Name']
        self.Priority = jsonData['Priority']
        if 'TreeRootName' in jsonData:
            self.TreeRootName = jsonData['TreeRootName']

        if 'AddInfluenceFactors' in jsonData:
            self.AddedInfluenceFactors = jsonData['AddInfluenceFactors']

        if 'RemoveInfluenceFactors' in jsonData:
            self.RemovedInfluenceFactors = jsonData['RemoveInfluenceFactors']

        if 'Selectors' in jsonData:
            for item in jsonData['Selectors']:
                tmp = Selector()
                tmp.fromJson(item)
                self.Selectors.append(tmp)

        if 'BehaviorVariableOverrides' in jsonData:
            for item in jsonData['BehaviorVariableOverrides']:
                tmp = BehaviourVariable()
                tmp.fromOverrideJson(jsonData['BehaviorVariableOverrides'][item], item)
                self.BehaviourVars.append(tmp)

    def toJson(self):
        ret = OrderedDict()
        ret['Name'] = self.Id
        ret['Priority'] = self.Priority
        ret['Selectors'] = []
        for item in self.Selectors:
            ret['Selectors'].append(item.toJson())
        if self.TreeRootName != '':
            ret['TreeRootName'] = self.TreeRootName

        ret['BehaviorVariableOverrides'] = OrderedDict()
        for item in self.BehaviourVars:
            data = item.toOverrideJson()
            ret['BehaviorVariableOverrides'][data[1]] = data[0]

        ret['AddInfluenceFactors'] = self.AddedInfluenceFactors
        ret['RemoveInfluenceFactors'] = self.RemovedInfluenceFactors

        return ret








