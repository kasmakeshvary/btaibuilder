from collections import OrderedDict


class Selector(object):

    def __init__(self):
        self.TypeName = ''
        self.SelectString = ''

    def toJson(self):
        data = OrderedDict()
        data['TypeName'] = self.TypeName
        data['SelectString'] = self.SelectString
        return data

    def fromJson(self, data):
        """

        :param data:
        :type data: OrderedDict
        :return:
        :rtype:
        """

        self.TypeName = data['TypeName']
        self.SelectString = data['SelectString']
