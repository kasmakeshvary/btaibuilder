from .treelookupitem import TreeLookupItem
from .behaviournodeitem import BehaviourTreeNode

class TreeStore(object):

    def __init__(self):
        self.trees = [] # type: list[TreeLookupItem]

    def addTree(self, tree):
        """

        :param tree:
        :type tree: BehaviourTreeNode
        :return:
        :rtype: bool
        """
        for item in self.trees:
            if item.treeName == tree:
                return False
        treelut = TreeLookupItem(tree)
        self.trees.append(treelut)
        return True

    def addRole(self, role, treeName):
        for tree in self.trees:
            if tree.treeName == treeName:
                tree.addRole(role)
                return True
        return False

    @property
    def roleList(self):
        ret = []
        for tree in self.trees:
            ret += tree.roleList
        return ret

    def treeList(self, filter=None):
        ret = []
        if filter is not None:
            for tree in self.trees:
                if tree.hasRole(filter):
                    ret.append(tree.treeName)
        else:
            for tree in self.trees:
                ret.append(tree.treeName)
        return ret