from .enodetypes import ENodeTypes
from collections import OrderedDict
import json

class BehaviourTreeNode(object):

    NodeType = ENodeTypes.Undefined

    def __init__(self, name, typename, extradata=None, parent=None):
        self.Name = name
        self.TypeName = typename
        self.ExtraParameters = extradata # type: OrderedDict
        self.ParentNode = parent # type: BehaviourTreeNode
        self.ChildrenNodes = [] # type: list[BehaviourTreeNode]
        if self.ExtraParameters is None:
            self.ExtraParameters = OrderedDict()

    @property
    def canHaveChildren(self):
        if self.NodeType in [ENodeTypes.Composite, ENodeTypes.Decorator, ENodeTypes.Root]:
            return True
        return False

    @property
    def maxChildren(self):
        if self.NodeType == ENodeTypes.Composite:
            return 9999999
        elif self.NodeType in [ENodeTypes.Decorator, ENodeTypes.Root]:
            return 1
        return 0

    def child(self, row):
        return self.ChildrenNodes[row]

    def childCount(self):
        return len(self.ChildrenNodes)

    def childNumber(self):
        if self.ParentNode != None:
            return self.ParentNode.ChildrenNodes.index(self)
        return 0

    def columnCount(self):
        return 2

    def insertChild(self, name, typename, nodetype, position, extradata=None):
        if position < 0 or position > len(self.ChildrenNodes):
            return False
        if not self.canHaveChildren:
            return False

        newNode = NODEMAP[nodetype]
        node = newNode(name, typename, extradata, parent=self)
        self.ChildrenNodes.insert(position, node)

        return True

    def insertItem(self, item, position):
        if position < 0 or position > len(self.ChildrenNodes):
            return False
        if not self.canHaveChildren:
            return False
        self.ChildrenNodes.insert(position, item)

    def popChild(self, position):
        if not self.canHaveChildren:
            return
        if position > len(self.ChildrenNodes):
            return
        return self.ChildrenNodes.pop(position)

    def addChild(self, name, typename, nodetype, extradata=None):
        if not self.canHaveChildren:
            return False

        newNode = NODEMAP[nodetype]
        node = newNode(name, typename, extradata, parent=self)
        self.ChildrenNodes.append(node)

        return True

    def removeChild(self, position):
        if not self.canHaveChildren:
            return False
        if position > len(self.ChildrenNodes):
            return False
        self.ChildrenNodes.pop(position)
        return True

    def parent(self):
        return self.ParentNode

    def row(self):
        if self.ParentNode:
            return self.ParentNode.ChildrenNodes.index(self)

        return 0

    def findNodeType(self, nodeName, nodeTypeName, nodedef, unknown):
        nodeType = CompositeNode
        if nodedef is not None:
            if nodeTypeName in nodedef['CompositeNodes']:
                nodeType = CompositeNode
            elif nodeTypeName in nodedef['DecoratorNodes']:
                nodeType = DecoratorNode
            elif nodeTypeName in nodedef['LeafNodes']:
                nodeType = LeafNode
            else:
                nodeType = LeafNode
                if nodeTypeName not in unknown:
                    unknown.append(nodeTypeName)
        return nodeType(nodeName, nodeTypeName, parent=self)


    def fromJson(self, json_data, nodedef, unknown):
        if self.NodeType == ENodeTypes.Root:
            self.ChildrenNodes = []
            node = self.findNodeType(json_data['Name'], json_data['TypeName'], nodedef, unknown)
            node.fromJson(json_data, nodedef, unknown)
            self.ChildrenNodes.append(node)
            return
        self.Name = json_data['Name']
        self.TypeName = json_data['TypeName']
        self.ChildrenNodes = []
        if 'ExtraParameters' in json_data:
            self.ExtraParameters = json_data['ExtraParameters']
        if 'Children' in json_data:
            if not self.canHaveChildren:
                raise Exception('Leaf Node cannot have children: {0}'.format(self.TypeName))
            for child in json_data['Children']:
                newNode = self.findNodeType(child['Name'], child['TypeName'], nodedef, unknown)
                newNode.fromJson(child, nodedef, unknown)
                self.ChildrenNodes.append(newNode)

    def toJson(self):
        if self.NodeType == ENodeTypes.Root:
            return self.ChildrenNodes[0].toJson()
        jsonData = OrderedDict()
        jsonData['Name'] = self.Name
        jsonData['TypeName'] = self.TypeName
        if len(self.ExtraParameters.keys()) > 0:
            jsonData['ExtraParameters'] = self.ExtraParameters
        if len(self.ChildrenNodes) > 0:
            jsonData['Children'] = []
            for child in self.ChildrenNodes:
                jsonChild = child.toJson()
                jsonData['Children'].append(jsonChild)
        return jsonData

    def loadJsonFile(self, filePath, nodedef, unknownNodes):
        try:
            fp = open(filePath, 'r')
            jsonData = json.load(fp, object_pairs_hook=OrderedDict, object_hook=OrderedDict)
            fp.close()
            self.fromJson(jsonData, nodedef, unknownNodes)
            return True
        except Exception as e:
            print(e)
            raise

        return False



class CompositeNode(BehaviourTreeNode):

    NodeType = ENodeTypes.Composite


class DecoratorNode(BehaviourTreeNode):

    NodeType = ENodeTypes.Decorator


class LeafNode(BehaviourTreeNode):

    NodeType = ENodeTypes.Leaf

class RootNode(BehaviourTreeNode):

    NodeType =  ENodeTypes.Root

NODEMAP = {
    ENodeTypes.Composite : CompositeNode,
    ENodeTypes.Decorator : DecoratorNode,
    ENodeTypes.Leaf : LeafNode
}