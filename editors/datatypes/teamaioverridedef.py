from collections import OrderedDict
from .selector import Selector


class TeamAiOverrideDef(object):

    TurnOrderWeightFactorItems = [
        'DistanceAlongPatrolRoute',
        'DistanceToClosestEnemy',
        'DistanceToClosestVulnerableEnemy',
        'IsUnstable',
        'IsVulnerable',
        'WalkSpeed'
    ]

    def __init__(self):
        self.Id = ''
        self.Priority = 0
        self.Selectors = []  # type: list[Selector]
        self.DesignateTargets = False
        self.TurnOrderFactorWeights = {}
        self.BehaviorScopesDirectory = ''

    def fromJson(self, jsonData):
        """

        :param jsonData:
        :type jsonData: OrderedDict
        :return:
        :rtype:
        """
        self.Id = jsonData['Name']
        self.Priority = jsonData['Priority']
        if 'TurnOrderFactorWeights' in jsonData:
            self.TurnOrderFactorWeights = jsonData['TurnOrderFactorWeights']

        if 'BehaviorScopesDirectory' in jsonData:
            self.BehaviorScopesDirectory = jsonData['BehaviorScopesDirectory']

        if 'DesignateTargets' in jsonData:
            self.DesignateTargets = jsonData['DesignateTargets']

        if 'Selectors' in jsonData:
            for item in jsonData['Selectors']:
                tmp = Selector()
                tmp.fromJson(item)
                self.Selectors.append(tmp)

    def toJson(self):
        ret = OrderedDict()
        ret['Name'] = self.Id
        ret['Priority'] = self.Priority
        ret['Selectors'] = []
        for item in self.Selectors:
            ret['Selectors'].append(item.toJson())
        ret['DesignateTargets'] = self.DesignateTargets
        if self.BehaviorScopesDirectory != '':
            ret['BehaviorScopesDirectory'] = self.BehaviorScopesDirectory
        if len(self.TurnOrderFactorWeights) > 0:
            ret['TurnOrderFactorWeights'] = self.TurnOrderFactorWeights
        return ret
