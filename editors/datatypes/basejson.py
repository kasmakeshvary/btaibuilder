import json
import json5
from collections import OrderedDict
import os
import os.path


class jsonReader(object):

    @staticmethod
    def readJsonFile(str_path):
        if not os.path.isfile(str_path):
            return None
        data = None
        with open(str_path, 'rb') as f:
            try:
                try:
                    data = json.load(f, object_pairs_hook=OrderedDict, object_hook=OrderedDict)
                except Exception as e:
                    f.seek(0)
                    data = json5.load(f, object_pairs_hook=OrderedDict, object_hook=OrderedDict)
            except Exception as e:
                data = None
        return data