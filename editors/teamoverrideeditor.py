from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

import sys
import json
import time
import os
import os.path
import pprint
import copy
from .forms import Ui_Form_TeamOverrideEdit
from .editorIds import EditorIds
from .baseeditor import BaseTabWidget
from .models import SelectorModel, TurnOrderWeightFactorModel
from .datatypes import jsonReader, TeamAiOverrideDef
from .dialogs import NewSelectorDialog
import traceback


class TeamOverrideEditor(BaseTabWidget, Ui_Form_TeamOverrideEdit):

    """
    the inventory tab handler

    """

    def __init__(self, obj_settings, bkprocessor, bol_debug=False, parent=None):
        """

        :param obj_settings: the preference settings
        :type obj_settings:
        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(TeamOverrideEditor, self).__init__(obj_settings, bkprocessor, bol_debug, parent)
        self.setupUi(self)
        self.Id = EditorIds.TeamOverrideTabId
        self.CurrentOverrideDef = TeamAiOverrideDef()
        self.selectorDataModel = SelectorModel()
        self.TurnOrderModel = TurnOrderWeightFactorModel()

        self.tableView_turnOrderFactors.setModel(self.TurnOrderModel)
        self.tableView_turnOrderFactors.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.tableView_turnOrderFactors.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tableView_turnOrderFactors.setContextMenuPolicy(Qt.ActionsContextMenu)

        actionRemove = QAction(self.tableView_turnOrderFactors)
        actionRemove.setText('Remove Selected Item(s)')
        actionRemove.triggered.connect(self.handleTurnOrderFactorRemove)
        self.tableView_turnOrderFactors.addAction(actionRemove)

        actionAdd = QAction(self.tableView_turnOrderFactors)
        actionAdd.setText('Add New Weight Factor')
        actionAdd.triggered.connect(self.newTurnOrderFactor)
        self.tableView_turnOrderFactors.addAction(actionAdd)

        self.tableView_Selectors.setModel(self.selectorDataModel)
        self.tableView_Selectors.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.tableView_Selectors.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)
        self.tableView_Selectors.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tableView_Selectors.setContextMenuPolicy(Qt.ActionsContextMenu)

        actionRemove = QAction(self.tableView_Selectors)
        actionRemove.setText('Remove Selected Item(s)')
        actionRemove.triggered.connect(self.handleSelectorRemove)
        self.tableView_Selectors.addAction(actionRemove)

        actionAdd = QAction(self.tableView_Selectors)
        actionAdd.setText('Add New Selector')
        actionAdd.triggered.connect(self.newSelector)
        self.tableView_Selectors.addAction(actionAdd)

        self.pushButton_LoadFile.pressed.connect(self.loadDef)
        self.pushButton_Save.pressed.connect(self.saveDef)

        # Hide this for now, not sure its needed
        self.pushButton_SelectBVarDir.setVisible(False)

        self.comboBox_Files.currentIndexChanged.connect(self.fileActivated)

        self.reload()


    def reload(self):
        self.comboBox_Files.clear()
        self.comboBox_Files.addItems(self.bkProcessor.CurrentMod.teamOverrideIds)


    def fileActivated(self):
        override = self.bkProcessor.CurrentMod.getTeamOverride(self.comboBox_Files.currentText())
        if override is not None:
            new_override = copy.deepcopy(override)
            self.activateOverrideDef(new_override)

    def activateOverrideDef(self, overridedef):
        """

        :param overridedef:
        :type overridedef: TeamAiOverrideDef
        :return:
        :rtype:
        """

        self.CurrentOverrideDef = overridedef
        self.lineEdit_Id.setText(self.CurrentOverrideDef.Id)
        self.lineEdit_BVarDir.setText(self.CurrentOverrideDef.BehaviorScopesDirectory)
        self.spinBox_Priority.setValue(self.CurrentOverrideDef.Priority)
        self.checkBox_DesignateTargets.setChecked(self.CurrentOverrideDef.DesignateTargets)
        self.TurnOrderModel.update_table(self.CurrentOverrideDef.TurnOrderFactorWeights)
        self.selectorDataModel.update_table(self.CurrentOverrideDef.Selectors)

    def saveDef(self):
        try:
            str_to_save = QFileDialog.getSaveFileName(self, caption="Save Team AI Override Def", filter="Team AI Override Def (*.json)", dir=self.Settings.LastModPath + '/{0}.json'.format(self.lineEdit_Id.text()))[0]

            if str_to_save != '':
                id = self.lineEdit_Id.text()
                self.CurrentOverrideDef.Id = self.lineEdit_Id.text()
                self.CurrentOverrideDef.BehaviorScopesDirectory = self.lineEdit_BVarDir.text()
                self.CurrentOverrideDef.Priority = self.spinBox_Priority.value()
                self.CurrentOverrideDef.Selectors = self.selectorDataModel.inventoryRecords
                self.CurrentOverrideDef.TurnOrderFactorWeights = self.TurnOrderModel.inventoryRecords
                self.CurrentOverrideDef.DesignateTargets = self.checkBox_DesignateTargets.isChecked()
                mfile = open(str_to_save, 'w')
                data = self.CurrentOverrideDef.toJson()
                json.dump(data, mfile, indent=4)
                mfile.close()
                self.bkProcessor.CurrentMod.addTeamOverrides(self.CurrentOverrideDef)
                self.reload()
                idex = self.bkProcessor.CurrentMod.teamOverrideIds.index(id)
                self.comboBox_Files.setCurrentIndex(idex)
                QMessageBox.information(self, 'Operation Success', 'Saved Unit AI Override Def')


        except Exception:
            print(traceback.format_exc())
            QMessageBox.critical(self, 'Operation Failed', 'Failed to Unit AI Override Def')

    def loadDef(self):
        try:
            str_file_to_load = QFileDialog.getOpenFileName(self, caption="Load TeamAiOverrideDef File",
                                                           filter="TeamAiOverrideDef(*.json)")[0]
            if str_file_to_load != '':
                data = jsonReader.readJsonFile(str_file_to_load)
                if data:
                    newDef = TeamAiOverrideDef()
                    newDef.fromJson(data)
                    self.activateOverrideDef(newDef)
                else:
                    raise Exception('Read Failure, check Json file is valid')
                QMessageBox.information(self, 'Operation Success', 'Loaded TeamAiOverrideDef')
        except Exception as e:
            print(traceback.format_exc())
            QMessageBox.critical(self, 'Operation Failed', str(e))

    def handleSelectorRemove(self):
        lst_idx = []
        for item in sorted(self.tableView_Selectors.selectedIndexes()):
            # lst_idx.append(self.proxyModel.mapToSource(item))
            lst_idx.append(item)
        if len(lst_idx) > 0:
            self.selectorDataModel.deleteItems(lst_idx)

    def newSelector(self):
        dialog = NewSelectorDialog()
        dialog.setWindowIcon(self.windowIcon())
        result = dialog.exec_()
        if result == QDialog.Accepted:
            self.selectorDataModel.addItem(dialog.lineEdit_Type.text(), dialog.lineEdit_Value.text())

    def handleTurnOrderFactorRemove(self):
        lst_idx = []
        for item in sorted(self.tableView_turnOrderFactors.selectedIndexes()):
            # lst_idx.append(self.proxyModel.mapToSource(item))
            lst_idx.append(item)
        if len(lst_idx) > 0:
            self.TurnOrderModel.deleteItems(lst_idx)

    def newTurnOrderFactor(self):
        keys = sorted(TeamAiOverrideDef.TurnOrderWeightFactorItems, key=str.lower)
        for item in self.TurnOrderModel.inventoryRecords.keys():
            keys.remove(item)
        idex = 0
        output = QInputDialog.getItem(self, 'Select Factor', 'Factor:\t\t\t\t', keys, current=idex,
                                      editable=False)
        if output[1]:
            self.TurnOrderModel.addItem(output[0], 0.0)
