from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from ..forms import Ui_Dialog_NewNodeDrop


class NewNodeDialog(QDialog, Ui_Dialog_NewNodeDrop):

    """
    the generic array tab handler

    """

    def __init__(self, nodedef, maxposition, title, compositeOnly=False, parent=None):
        """

        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(NewNodeDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(title)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.NodeDef = nodedef
        self.spinBox_Position.setMaximum(maxposition)
        self.spinBox_Position.setValue(maxposition)

        self.comboBox_Type.currentIndexChanged.connect(self.updateTypeText)
        self.comboBox_Node.currentIndexChanged.connect(self.updateTypes)
        if compositeOnly:
            self.comboBox_Node.addItems(['Composite Node'])
        else:
            self.comboBox_Node.addItems(['Composite Node', 'Decorator Node', 'Leaf Node'])
        self.comboBox_Node.setCurrentIndex(0)

    def updateTypes(self):
        txt = self.comboBox_Node.currentText().replace(" ", "")
        vals = self.NodeDef[txt + "s"]
        self.comboBox_Type.clear()
        self.comboBox_Type.addItems(vals)

    def updateTypeText(self):
        self.lineEdit_Type.setText(self.comboBox_Type.currentText())
