from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

import sys
import json
import time
import os
import os.path
import pprint
from .forms import Ui_FormBVarsEditor
from .editorIds import EditorIds
from .baseeditor import BaseTabWidget
from .models import BehaviourVariableModel, BVarProxyModel
from .datatypes import jsonReader
from .dialogs import NewBVarDialog
import traceback


class BVarEditor(BaseTabWidget, Ui_FormBVarsEditor):

    """
    the inventory tab handler

    """

    def __init__(self, obj_settings, bkprocessor, bol_debug=False, parent=None):
        """

        :param obj_settings: the preference settings
        :type obj_settings:
        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(BVarEditor, self).__init__(obj_settings, bkprocessor, bol_debug, parent)
        self.setupUi(self)
        self.Id = EditorIds.BVarTabId
        self.setWindowTitle('Behaviour Variable Editor')
        self.dataModel = BehaviourVariableModel()
        # Hide this until we see if its needed
        self.pushButton_New.setVisible(False)
        self.pushButton_Load.pressed.connect(self.handleLoad)
        self.pushButton_Save.pressed.connect(self.handleSave)

        # self.proxyModel = BVarProxyModel()
        # self.proxyModel.setSourceModel(self.dataModel)

        # set up the inventory table
        self.tableView.setModel(self.dataModel)
        # self.tableView.verticalHeader().show()
        self.tableView.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.tableView.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tableView.setContextMenuPolicy(Qt.ActionsContextMenu)

        actionRemove = QAction(self.tableView)
        actionRemove.setText('Remove Selected Item(s)')
        actionRemove.triggered.connect(self.handleRemove)
        self.tableView.addAction(actionRemove)

        actionAdd = QAction(self.tableView)
        actionAdd.setText('Add New Behaviour Var')
        actionAdd.triggered.connect(self.newBVar)
        self.tableView.addAction(actionAdd)

    def resizeEvent(self, event):
        """
        handle resize events so that the table stays nice

        :param event:
        :type event:
        :return:
        :rtype:
        """
        self.tableView.setColumnWidth(1, self.tableView.width()/4)

        QWidget.resizeEvent(self, event)

    def handleLoad(self):
        try:
            str_file_to_load = QFileDialog.getOpenFileName(self, caption="Load Behaviour Variable File",
                                                           filter="AI BehaviourVars (*.json)")[0]
            if str_file_to_load != '':
                data = jsonReader.readJsonFile(str_file_to_load)
                if data:
                    self.dataModel.update_table(data)
                else:
                    raise Exception('Read Failure, check Json file is valid')
                QMessageBox.information(self, 'Operation Success', 'Loaded Behaviour Variables')
        except Exception as e:
            print(traceback.format_exc())
            QMessageBox.critical(self, 'Operation Failed', str(e))

    def handleSave(self):
        try:
            str_to_save = QFileDialog.getSaveFileName(self, caption="Save Behaviour Variables", filter="AI BehaviourVars (*.json)")[0]

            if str_to_save != '':
                mfile = open(str_to_save, 'w')
                data = self.dataModel.export_table()
                json.dump(data, mfile, indent=4)
                mfile.close()
                QMessageBox.information(self, 'Operation Success', 'Saved Behaviour Variables')

        except Exception:
            print(traceback.format_exc())
            QMessageBox.critical(self, 'Operation Failed', 'Failed to save Behaviour Variables')


    def handleRemove(self):
        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            # lst_idx.append(self.proxyModel.mapToSource(item))
            lst_idx.append(item)
        if len(lst_idx) > 0:
            self.dataModel.deleteItems(lst_idx)

    def newBVar(self):
        dialog = NewBVarDialog()
        dialog.setWindowIcon(self.windowIcon())
        result = dialog.exec_()
        if result == QDialog.Accepted:
            ret = self.dataModel.addItem(dialog.lineEdit_Name.text(), dialog.comboBox_Type.currentText(), dialog.lineEdit_Value.text())
            if not ret:
                QMessageBox.critical(self, 'Operation Failed', 'Value: {0}, is not of Type: {1}'.format(dialog.lineEdit_Value.text(), dialog.comboBox_Type.currentText()))
