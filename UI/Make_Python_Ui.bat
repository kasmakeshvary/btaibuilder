@echo off
del ..\maineditor.py
del ..\editors\forms\TreeEditor.py
del ..\editors\forms\bvarseditor.py
del ..\editors\forms\newNodeDrop.py
del ..\editors\forms\unitoverrideedit.py
del ..\editors\forms\teamoverrideedit.py
del ..\editors\forms\newbvardialog.py
del ..\editors\forms\arrayeditorui.py
del ..\editors\forms\newselector.py
del ..\images_rc.py
del ..\stylesheets_rc.py
pyside2-uic.exe -o ..\maineditor.py .\maineditor.ui
pyside2-uic.exe -o ..\editors\forms\newNodeDrop.py .\newNodeDrop.ui
pyside2-uic.exe -o ..\editors\forms\TreeEditor.py .\TreeEditor.ui
pyside2-uic.exe -o ..\editors\forms\bvarseditor.py .\bvarseditor.ui
pyside2-uic.exe -o ..\editors\forms\unitoverrideedit.py .\unitoverrideedit.ui
pyside2-uic.exe -o ..\editors\forms\newbvardialog.py .\newbvardialog.ui
pyside2-uic.exe -o ..\editors\forms\arrayeditorui.py .\arrayeditorui.ui
pyside2-uic.exe -o ..\editors\forms\newselector.py .\newselector.ui
pyside2-uic.exe -o ..\editors\forms\teamoverrideedit.py .\teamoverrideedit.ui
pyside2-rcc.exe -o ..\images_rc.py .\images.qrc
pyside2-rcc.exe -o ..\stylesheets_rc.py .\stylesheets.qrc