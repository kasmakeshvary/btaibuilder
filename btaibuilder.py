from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import  *


import sys
import time
import os
import os.path
import logging
import logging.handlers

from maineditor import Ui_BtAiBuilder
from editors import EditorIds, TreeEditor, AiBuilderSettings, BVarEditor, UnitOverrideEditor, TeamOverrideEditor, BkProcessor


STR_VERSION = '201.0.0-20190902'

rootLogger = logging.getLogger('BTAi')
rootLogger.setLevel(logging.INFO)
obj_handler = logging.FileHandler('BtAiBuildLog.txt', mode='w')
obj_formatter = logging.Formatter('%(levelname)-10s (%(module)-20s:%(funcName)-30s:%(lineno)-4d) %(message)s')
obj_handler.setFormatter(obj_formatter)
obj_handler.setLevel(logging.DEBUG)
rootLogger.addHandler(obj_handler)


class BattleTechAiBuilder(QMainWindow, Ui_BtAiBuilder):
    """
    Main Editor UI

    """

    SettingsFile = 'BtAiBuilderSettings.xml'
    Signal_ScanDone = Signal(bool)

    def __init__(self, obj_qapp, parent=None):
        super(BattleTechAiBuilder, self).__init__(parent)
        self.setupUi(self)
        self.qapp = obj_qapp
        self.setMinimumHeight(480)
        self.setMinimumWidth(640)
        self.setWindowTitle('BattleTech AI Builder v' + STR_VERSION)
        self.ProgressDialog = None
        self.Settings = AiBuilderSettings()
        self.debugFlag = False
        if os.path.exists(self.SettingsFile):
            self.Settings.fromFile(self.SettingsFile)
        else:
            self.Settings.toFile(self.SettingsFile)

        self.menuEditors = QMenu('Editors')
        self.menubar.addMenu(self.menuEditors)
        tabs = [EditorIds.AiTabId, EditorIds.BVarTabId, EditorIds.UnitOverrideTabId, EditorIds.TeamOverrideTabId]
        for id in tabs:
            actionInventory = QAction(self.menuEditors)
            actionInventory.setText(id)
            actionInventory.triggered.connect(self.activateEditor)
            self.menuEditors.addAction(actionInventory)

        self.styleSkins = ['No Skin', 'QDarkStyle', 'Fusion', 'Pone']

        self.menuThemes = QMenu('Themes')
        for id in self.styleSkins:
            actionInventory = QAction(self.menuThemes)
            actionInventory.setText(id)
            actionInventory.triggered.connect(self.update_skin)
            self.menuThemes.addAction(actionInventory)

        self.themeMenuItem = self.menubar.addMenu(self.menuThemes)
        self.themeMenuItem.setVisible(True)

        self.set_skin('Fusion')  # set the default skin

        self.pushButton_Load.pressed.connect(self.scan_pressed)

        self.mdiArea.setViewMode(QMdiArea.TabbedView)
        self.mdiArea.setTabsClosable(True)
        self.mdiArea.setDocumentMode(True)

        self.bkThread = QThread()
        self.bkProcessor = BkProcessor(rootLogger)
        self.bkProcessor.moveToThread(self.bkThread)
        self.bkThread.start()

        self.bkProcessor.FileScanComplete.connect(self._scanComplete)

        obj_tab = TreeEditor(self.Settings, self.bkProcessor)
        self.mdiArea.addSubWindow(obj_tab, Qt.SubWindow)
        obj_tab.showMaximized()

    def setPonePalette(self):
        palette = QPalette()
        palette.setColor(QPalette.Window, QColor(245, 17, 120))
        palette.setColor(QPalette.WindowText, Qt.white)
        palette.setColor(QPalette.Base, QColor(212, 23, 152))
        palette.setColor(QPalette.AlternateBase, QColor(245, 17, 120))
        palette.setColor(QPalette.ToolTipBase, Qt.white)
        palette.setColor(QPalette.ToolTipText, Qt.white)
        palette.setColor(QPalette.Text, Qt.white)
        palette.setColor(QPalette.Button, QColor(245, 17, 120))
        palette.setColor(QPalette.ButtonText, Qt.white)
        palette.setColor(QPalette.BrightText, Qt.red)

        palette.setColor(QPalette.Highlight, QColor(206, 23, 212).lighter())
        palette.setColor(QPalette.HighlightedText, Qt.black)
        self.qapp.setPalette(palette)

    def setFusionPalette(self):
        palette = QPalette()
        palette.setColor(QPalette.Window, QColor(53, 53, 53))
        palette.setColor(QPalette.WindowText, Qt.white)
        palette.setColor(QPalette.Base, QColor(15, 15, 15))
        palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        palette.setColor(QPalette.ToolTipBase, Qt.white)
        palette.setColor(QPalette.ToolTipText, Qt.white)
        palette.setColor(QPalette.Text, Qt.white)
        palette.setColor(QPalette.Button, QColor(53, 53, 53))
        palette.setColor(QPalette.ButtonText, Qt.white)
        palette.setColor(QPalette.BrightText, Qt.red)

        palette.setColor(QPalette.Highlight, QColor(142, 45, 197).lighter())
        palette.setColor(QPalette.HighlightedText, Qt.black)
        self.qapp.setPalette(palette)

    def update_skin(self):
        """
        change the skin of the UI

        :return:
        :rtype:
        """
        str_skin = self.sender().text()
        self.set_skin(str_skin)

    def set_skin(self, str_skin):
        """
        apply a stylesheet operation

        :param str_skin: the skin to change to, eithr an option in self.skinStyles or a builtin one
        :type str_skin: str
        :return:
        :rtype:
        """
        # print 'Selecting Skin: {0}'.format(str_skin)
        bol_pone = False
        if str_skin == 'Pone':
            str_skin = 'Fusion'
            bol_pone = True
        if str_skin in QStyleFactory.keys():
            self.qapp.setStyle(QStyleFactory.create(str_skin))
            self.qapp.setStyleSheet("")
            if str_skin == 'Fusion':
                if bol_pone:
                    self.setPonePalette()
                else:
                    self.setFusionPalette()
            else:
                self.qapp.setPalette(self.style().standardPalette())
        else:
            self.qapp.setPalette(self.style().standardPalette())
            self.qapp.setStyle(QStyleFactory.create('windowsvista'))
            mfile = QFile(":/stylesheets/{0}".format(str_skin))
            mfile.open(QFile.ReadOnly)
            style = mfile.readAll()
            self.qapp.setStyleSheet(style.data().decode('utf8'))
            mfile.close()

    def activateEditor(self):
        """
        activate the correct editor tabe, or spawn it if its not open

        :return:
        :rtype:
        """
        str_tab = self.sender().text()
        lst_windows = self.mdiArea.subWindowList()
        for window in lst_windows:
            if window.widget().Id == str_tab:
                self.mdiArea.setActiveSubWindow(window)
                break
        else:
            obj_tab = None
            if str_tab == EditorIds.AiTabId:
                obj_tab = TreeEditor(self.Settings, self.bkProcessor, bol_debug=self.debugFlag)
            elif str_tab == EditorIds.BVarTabId:
                obj_tab = BVarEditor(self.Settings, self.bkProcessor, bol_debug=self.debugFlag)
            elif str_tab == EditorIds.UnitOverrideTabId:
                obj_tab = UnitOverrideEditor(self.Settings, self.bkProcessor, bol_debug=self.debugFlag)
            elif str_tab == EditorIds.TeamOverrideTabId:
                obj_tab = TeamOverrideEditor(self.Settings, self.bkProcessor, bol_debug=self.debugFlag)
            if obj_tab is not None:
                self.mdiArea.addSubWindow(obj_tab, Qt.SubWindow)
                obj_tab.showMaximized()

    def _scanComplete(self, bol_success):
        """
        handle updates to the UI when scanning is complete

        :param bol_success: whether the scan was successful or not
        :type bol_success: bool
        :return:
        :rtype:
        """
        if self.ProgressDialog is not None:
            self.ProgressDialog.reset()
        if not bol_success:
            QMessageBox.critical(self, 'Operation Failed!', 'Could not located JSON assets')
        else:
            self.Signal_ScanDone.emit(True)

    def scan_pressed(self):
        """
        handle scan pressed

        :return:
        :rtype:
        """
        str_file_to_load = QFileDialog.getExistingDirectory(self, caption="Mod Directory", dir=self.Settings.LastModPath)
        if str_file_to_load != '':
            self.Settings.LastModPath = str_file_to_load
            self.Settings.toFile(self.SettingsFile)
            self.bkProcessor.StartFileScan.emit(str_file_to_load)
            self.ProgressDialog = QProgressDialog('Scanning Mod', 'Cancel', 0, 0, self)
            self.ProgressDialog.setWindowFlags(self.ProgressDialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
            self.ProgressDialog.setLabelText('Scanning Mod...')
            self.ProgressDialog.setCancelButton(None)
            self.ProgressDialog.setWindowTitle('Scanning Mod')
            self.ProgressDialog.setWindowModality(Qt.WindowModal)
            self.ProgressDialog.showNormal()

    def closeEvent(self, event):
        """
        handle shutdown gracefully

        :param event:
        :type event:
        :return:
        :rtype:
        """

        # Kill the background thread or Qt will get mad
        self.bkThread.terminate()
        while not self.bkThread.isFinished():
            time.sleep(0.05)
        event.accept()


if __name__ == '__main__':

    rootLogger.info('----- BT Save Editor v{0} -----'.format(STR_VERSION))

    obj_main_app = QApplication(sys.argv)
    obj_main_window = BattleTechAiBuilder(obj_main_app)
    obj_main_window.show()
    obj_main_app.exec_()
